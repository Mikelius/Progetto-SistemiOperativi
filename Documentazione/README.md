***Progetto-SistemiOperativi***
=========================

--------------------------------------

**_Cannavo' Michele - Matr. O46-002210_**

--------------------------------------

## Consegna1
------------------------------

### **1) Elenco funzioni**

|    **NOME**    | **DESCRIZIONE**                                  |
|---------------:|:-------------------------------------------------|
| **testPath()** | _Testa se la cartella iniziale esiste_           |
| **getNCore()** | _Reperisce il numero dei core di un processore_  |
